﻿#include <iostream>
#include "Helpers.h"

int main()
{
    setlocale(LC_ALL, "ru");

    int a, b;
    std::cout << "Введите число А:" << std::endl;
    std::cin >> a;
    std::cout << "Введите число B:" << std::endl;
    std::cin >> b;
    std::cout << "Сумма квадратов: " << sum(a, b) << std::endl;
    return 0;
}


